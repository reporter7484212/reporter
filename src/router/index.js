import { createRouter, createWebHistory } from 'vue-router'
// import HomeView from '@/views/HomeView.vue'
import LoginView from '@/views/LoginView.vue'
import SearchView from "@/views/SearchView.vue";
import RecordingView from "@/views/RecordingView.vue";
import ManagementView from "@/views/ManagementView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'HomeView',
      component: LoginView
    },
    {
      path: '/login',
      name: 'LoginView',
      component: LoginView
    },
    {
      path: '/search',
      name: 'SearchView',
      component: SearchView
    },
    {
      path: '/record',
      name: 'RecordingView',
      component: RecordingView

    },
    {
      path: '/management',
      name: 'ManagementView',
      component: ManagementView

    }
  ]
})

export default router
