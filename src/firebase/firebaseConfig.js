import {initializeApp} from "firebase/app";
import {getFirestore, setDoc, collection, doc, getDocs, query, where, getDoc, updateDoc} from "firebase/firestore";
import {getAuth,signInWithEmailAndPassword, signOut, createUserWithEmailAndPassword, updateProfile} from "firebase/auth";
import {getStorage, ref, uploadBytesResumable, getDownloadURL} from "firebase/storage";

const firebaseConfig = {
    apiKey: "AIzaSyBdg3S8XMyNiBNuBHGj8rpt6du2mBfTeDI",
    authDomain: "reporter-5d0d5.firebaseapp.com",
    projectId: "reporter-5d0d5",
    storageBucket: "reporter-5d0d5.appspot.com",
    messagingSenderId: "57007035403",
    appId: "1:57007035403:web:370b4976ce7faaabc9630e"
};

const app = initializeApp(firebaseConfig);

const db =  getFirestore(app);

const auth = getAuth(app);

const storage = getStorage(app);

const profilsRef = collection(db, "profils");

async function addUser(email, password, fullName, audio, company_id) {
  try {
    const audioURL = await uploadAudio(audio);
    const company_id = await getCompanyId();
    createUserWithEmailAndPassword(auth, email, password).then(async (userCredential) => {
      const user = userCredential.user;

      const profilsRef = db.collection("companies").doc(company_id).collection("users").doc(user.uid);
      setDoc(profilsRef, {
        name: fullName,
        url: audioURL,
      });
    });
  } catch (error) {
    console.error(error.message);
  }
}

async function getCompanyId() {
  try {
    if(auth.currentUser){
    const q = query(collection(db, "Companies"), where(`users.${auth.currentUser.uid}`, '!=', ''));
    const querySnapshot = await getDocs(q);

    if (!querySnapshot.empty) {
      getDoc
      const companyId = querySnapshot.docs[0].id;
      return companyId;
    } else {
      console.log("No company document found for the user");
    }
  }
  } catch (error) {
    console.error("Error getting company document:", error);
  }
}

async function checkAdmin() {
  try {
    const company_id = await getCompanyId();
    console.log(company_id)
    const companyRef = doc(db, "Companies", company_id);
    const companyDoc = await getDoc(companyRef);
    
    if (companyDoc.exists) {
      const companyData = companyDoc.data();
      
      if (companyData && companyData.admins && companyData.admins[auth.currentUser.uid]) {
        console.log("User is an admin.");
        return true;
      } else {
        console.log("User is not an admin.");
        return false;
      }
    } else {
      console.log("No company document found for the provided company ID");
      return false;
    }
  } catch (error) {
    console.error("Error checking admin status:", error);
    return false;
  }
}

async function getProfils() {
  let documents = [];
  try {
    const company_id = await getCompanyId();
    const companyRef = doc(db, "Companies", company_id);
    const companyDoc = await getDoc(companyRef);
    const users = companyDoc.data().users;
    
    Object.keys(users).forEach(key => {
      const obj = {};
      obj[key] = users[key];
      documents.push(obj);
    });
  } catch (error) {
    console.error('Error getting documents: ', error);
    throw error;
  }
  console.log(documents)
  return documents;
}

async function getMeetingsForCurrentUser() {
  try {
    const company_id = await getCompanyId();
    const companyRef = doc(db, "Companies", company_id);
    const companySnapshot = await getDoc(companyRef);
    
    if (!companySnapshot.exists()) {
      console.log("Company document does not exist");
      return []; // Return an empty array if company document doesn't exist
    }
    
    const companyData = companySnapshot.data();
    const meetings = companyData.meetings || {}; // Get meetings object, or empty object if not available
    
    const currentUserUid = auth.currentUser.uid;
    const userMeetings = [];
    
    Object.keys(meetings).forEach(meetingId => {
      const speakers = meetings[meetingId].speakers || {};
      if (speakers [currentUserUid]) {
        // Include only the required fields for the user meetings
        const { speakers, csv_file, title } = meetings[meetingId];
        userMeetings.push({ title, speakers, csv_file });
      }
    });
    
    console.log("User's meetings:", userMeetings);
    return userMeetings;
  } catch (error) {
    console.error("Error getting meetings:", error);
    return []; // Return empty array in case of error
  }
}


async function uploadAudio(audio) {
  let audioURL = "";
    const storageRef = ref(storage, `audio/${audio.name}`);
    
    try {
      audioURL = await uploadBytesResumable(storageRef, audio).then(async (snapshot) => {
      const URL = await getDownloadURL(snapshot.ref);
      return URL;
      });
    } catch (error) {
      console.error("Erreur lors de l'upload du document", error);
    }
  return audioURL;
}

async function uploadFile(file) {
  const storageRef = ref(storage, `csv/${generateRandomFileName()}.csv`);

  try {
    // Upload file with Content-Type metadata set to text/csv
    const snapshot = await uploadBytesResumable(storageRef, file, {
      contentType: 'text/csv'
    });
    
    // Get download URL
    const downloadURL = await getDownloadURL(snapshot.ref);
    
    return downloadURL;
  } catch (error) {
    console.error("Error uploading file:", error);
    throw error; // Propagate the error to the caller
  }
}

async function addMeeting(users, title, audioURL, file) {
  try {
    // Upload file
    const fileURL = await uploadFile(file);

    // Get company ID
    const company_id = await getCompanyId();

    // Get reference to company document
    const companyRef = doc(db, "Companies", company_id);

    // Generate a unique ID for the meeting
    const meeting_id = generateMeetingID();

    // Prepare participants object
    const speakers = {};
    for (let i in users) {
      for (const key in users[i]) {
        console.log("user", users[i][key].name)
        speakers[key] = users[i][key].name;
      }
    }

    // Construct the meeting object
    const meetingData = {
      title: title,
      audio_file: audioURL,
      csv_file: fileURL,
      speakers: speakers
    };

    // Add the meeting data to the meetings field in the company document
    await updateDoc(companyRef, {
      [`meetings.${meeting_id}`]: meetingData
    });

    console.log("Meeting added successfully");
  } catch (error) {
    // Handle errors
    console.error("Error adding meeting:", error);
    // You can throw the error again if you want to propagate it to the caller
    // throw error;
  }
}

function generateMeetingID() {
  // Generates a version 4 (random) UUID
  // For example: 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0,
        v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}


async function signIn(email, password) {
  try {
  return await signInWithEmailAndPassword(auth, email, password);
} catch (err) {
  console.log("Login Failed");
  console.log(err.code);
  console.log(err.message);
  return err.message;
}
}

function signOutUser() {
    signOut(auth)
    .then()
    .catch((err) => {
        console.log(err.code);
        console.log(err.message);
    });
}

function generateRandomFileName() {
  const timestamp = new Date().getTime();
  const randomString = Math.random().toString(36).substring(2, 8); // Generates a random string of length 6
  return `${timestamp}${randomString}`;
}

export {
    addUser,
    auth,
    signIn,
    signOutUser,
    db,
    getProfils,
    uploadAudio,
    checkAdmin,
    addMeeting,
    uploadFile,
    getMeetingsForCurrentUser,
};