import { defineStore } from "pinia";

export const myStore = defineStore("myStore", {
  state() {
    return {
      token: localStorage.getItem("token") || "",
    };
  },
  getters: {
    getToken() {
      return this.token !== "";
    },
  },
  actions: {
    setToken(token) {
      this.token = token;
      localStorage.setItem("token", token);
    },
  },
});